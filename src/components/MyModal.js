import React, { useState } from "react";
import { Button } from "flowbite-react";
import { XMarkIcon } from "@heroicons/react/24/outline";

export default function MyModal(props) {
    const [showModal, setShowModal] = useState(props.showModal);
    const toggleShow = () => {
        props.toggleShow();
    }

    return (
        <div onClick={toggleShow} className={`fixed inset-0 flex justify-center items-center transition-colors ${showModal ? "visible bg-black/20" : "invisible"}`}>
            <div onClick={(e) => e.stopPropagation()} className={`bg-white rounded-md shadow p-6 ${showModal ? "scale-100 opacity-100" : "scale-125 opacity-0"}`}>
                <Button size="xs" onClick={toggleShow} className="absolute top-2 right-2 p-2 rounded-lg text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 text-sm ml-auto inline-flex items-center dark:hover:bg-gray-600 dark:hover:text-white">
                    <XMarkIcon className="h-5 w-5 text-gray-500" />
                </Button>
                {props.children}
            </div>
        </div>
    )
}