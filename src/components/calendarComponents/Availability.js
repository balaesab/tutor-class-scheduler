import React, {Component} from 'react';
import '../.././styles/Calendar.css';
import {DayPilot, DayPilotCalendar, DayPilotNavigator} from "@daypilot/daypilot-lite-react";
import pb from '../../lib/pocketbase';
import { useEffect, useState, useRef } from 'react';

const styles = {
  wrap: {
    display: "flex"
  },
  left: {
    marginRight: "10px"
  },
  main: {
    flexGrow: "1"
  }
};

export default function Availaility() {
  const calendarRef = useRef(null);
  const [events, setEvents] = useState([]);
  const role = pb.authStore.model.role;

  const getCalendar = () => {
    return calendarRef.current.control;
  }

  const displayData = (data) => {
    const mappedEvents = data.map(event => ({
      id: event.id,
      start: new DayPilot.Date(event.start),
      end: new DayPilot.Date(event.end),
      text: "Volný slot",
      backColor: "#93c47d"
    }));
    return mappedEvents;
  }
  
  const getAvailability = async() => {
    return await pb.collection('availability').getFullList({filter: `creator="${pb.authStore.model.id}"`});
  }

  useEffect(() => {
    getAvailability().then(data => {
        displayData(data);
        setEvents(displayData(data));
      })
  }, [])

  const handleTimeRangeSelected = async (args) => {
    const dp = getCalendar();
    dp.clearSelection();
    
    const createEvent = async() => {
      await pb.collection('availability').create({
       start: args.start,
       end: args.end,
       creator: pb.authStore.model.id
     });
    }

    createEvent().then(() => {
      getAvailability().then(data => {
        displayData(data);
        setEvents(displayData(data));
      })
    })
  }

  const handleEventClick = async (args) => {
    console.log("click event");
    
  }

  const handleEventMoved = async (args) => {
    console.log("moving event");
    const e = args.e;
    await pb.collection('availability').update(e.data.id, {
      start: e.data.start,
      end: e.data.end
    })
  }

  const handleEventDelete = async (args) => {
    const eventId = args.e.data.id;
      try {
        await pb.collection("availability").delete(eventId);
      } catch (error) {
      return { error: error.message };
      }
      console.log("deleting event");
    }

    const handleEventResized = async (args) => {
      console.log("resizing event");
      const e = args.e;
      await pb.collection('availability').update(e.data.id, {
        start: e.data.start,
        end: e.data.end
      })
    }

    if (localStorage.getItem('selectedDateAvail') === null) {
      localStorage.setItem('selectedDateAvail', new DayPilot.Date());
    }
    return (
      <div style={styles.wrap} className="cal">
        <div style={styles.left}>
          <DayPilotNavigator
            selectMode={"Week"}
            weekStarts = {1}
            locale = 'cs-CZ'
            showMonths={2}
            skipMonths={2}
            startDate={new DayPilot.Date(localStorage.getItem('selectedDateAvail'))}
            onTimeRangeSelected={ args => {
              localStorage.setItem('selectedDateAvail', args.start.value);
              getCalendar().update({
                startDate: localStorage.getItem('selectedDateAvail')
              });
            }}
          />
        </div>
        <div style={styles.main}>
          <DayPilotCalendar
            events={events}
            ref={calendarRef}
            viewType = {"Week"}
            weekStarts = {1}
            locale = 'cs-CZ'
            headerDateFormat = {'dddd d.M.yyyy'}
            durationBarVisible = {false}
            timeRangeSelectedHandling = {"Enabled"}
            eventDeleteHandling = {"Update"}
            onTimeRangeSelected={handleTimeRangeSelected}
            onEventDelete={handleEventDelete}
            onEventClicked={handleEventClick}
            onEventMoved={handleEventMoved}
            onEventResized={handleEventResized}
          />
        </div>
      </div>
    );
}
