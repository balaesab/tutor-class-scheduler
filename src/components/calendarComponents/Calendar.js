import React from 'react';
import '../.././styles/Calendar.css';
import {DayPilot, DayPilotCalendar, DayPilotNavigator} from "@daypilot/daypilot-lite-react";
import pb from '../../lib/pocketbase';
import { useEffect, useState, useRef } from 'react';
import CreateEvent from '../events/CreateEvent.js'
import { useParams } from "react-router-dom";
import { getTime, getParticipantsNames } from '../../utils/format.js';
import ViewEvent from '../events/ViewEvent';

const styles = {
  wrap: {
    display: "flex"
  },
  left: {
    marginRight: "10px",
    borderRadius: '0.5rem'
  },
  main: {
    flexGrow: "1"
  },
};

export default function CalendarComponent({}) {
  const calendarRef = useRef(null);
  const [events, setEvents] = useState([]);
  const [availability, setAvailability] = useState([]);
  const [overlapAvail, setOverlapAvail] = useState('');
  const [data, setData] = useState('');
  const [overlapEvents, setOverlapEvents] = useState('');
  const [showModal, setShowModal] = useState(false);
  const [showOnClickModal, setShowOnClickModal] = useState(false);
  const role = pb.authStore.model.role;
  const {id} = useParams();
  const [args, setArgs] = useState("");

  const getCalendar = () => {
    return calendarRef.current.control;
  }

  const getEvents = async() => {
    if (role === "tutor") {
      return await pb.collection('events').getFullList({
        sort: '-created', expand: 'participants, lesson', filter: `creator="${pb.authStore.model.id}"`
      });
    }
    else if (role === "student") {
      if(id) {
        return await pb.collection('events').getFullList({expand: 'participants, lesson, creator',filter: `creator="${id}"`});
      }
      return await pb.collection('events').getFullList({expand: 'participants, lesson, creator', filter: `participants.id?="${pb.authStore.model.id}"`});
    }
  }

  const displayData = (data) => {
    const currUser = pb.authStore.model.id;   
    const mappedEvents = data.map(event => ({
      id: event.id,
      start: new DayPilot.Date(event.start),
      end: new DayPilot.Date(event.end),
      text: role==="student" ? getTime(new DayPilot.Date(event.start)) + "\n" + event.expand.lesson.name + "\n" + event.expand.creator.name : getTime(new DayPilot.Date(event.start)) + "\n" + event.expand.lesson.name + "\n" + getParticipantsNames(event.expand.participants),
      editable: true,
      creator: event.creator,
      participants: event.expand.participants,
      lesson: event.expand.lesson,
      moveDisabled: true,
      resizeDisabled: true,
      deleteDisabled: event.creator===currUser || !event.expand.participants.some(participant => participant.id === currUser),
      backColor: !event.expand.participants.some(participant => participant.id === currUser) ? "#fa993e" : "#2e78d6",
    }));
    return mappedEvents;
  }

  useEffect(() => {
    getEvents().then(data => {
      displayData(data);
      setEvents(displayData(data));
    })
  }, [id]);

  useEffect(() => {
    const getAvailability = async() => {
      if (role === "tutor") {
        return await pb.collection('availability').getFullList({filter: `creator="${pb.authStore.model.id}"`});
      }
      else if (role === "student") {
        if (id) {
          return await pb.collection('availability').getFullList({filter: `creator="${id}"`});
        }
      }
    }
    getAvailability().then(data => {
      const mappedAvail = data && data.map(event => ({
        id: event.id,
        start: new DayPilot.Date(event.start),
        end: new DayPilot.Date(event.end),
        text: "Volný slot",
        backColor: "#93c47d",
        editable: false,
        moveDisabled: true,
        deleteDisabled: true,
        resizeDisabled: true
      }));
      setAvailability(mappedAvail);
    })
  }, [id])

  const newEvent = (lesson) => {
    if (lesson) {
      const parsedLesson = JSON.parse(lesson);
      console.log("vytvareni eventu")
      const start = new DayPilot.Date(args.start);
      const end = start.addHours(parsedLesson.duration).toString();
      
      const createEvent = async() => {
          return await pb.collection('events').create({
          start: args.start,
          end: end,
          creator: id,
          participants: pb.authStore.model.id,
          text: getTime(new DayPilot.Date(args.start)) + "\n" + lesson.name + "\n" + pb.authStore.model.name,
          lesson: parsedLesson.id
        })
      }
      createEvent().then(() => {
          getEvents().then((data) => {
          displayData(data);
          setEvents(displayData(data));
        })
      })
      toggleShow();
    }
    else {
      console.log("Not creating event");
    }
  }

  const handleTimeRangeSelected = async (args) => {    
    setData(args);
    if (role === "student") {
      //filter out same day availability as the one its about to create
      const sameDayAvail = availability.filter((avail) => {
        return avail.start.getDatePart() === args.start.getDatePart();
      })
      console.log(sameDayAvail);

      const sameDayEvents = events.filter((avail) => {
        return avail.start.getDatePart() === args.start.getDatePart();
      })
      setOverlapEvents(sameDayEvents);
      
      //if theres availability the same day, then check the range of the availability
      if (sameDayAvail.length > 0) {
        let overlapAvail = sameDayAvail.some(avail => args.start < avail.end && args.end > avail.start);
        setOverlapAvail(sameDayAvail.find(avail => args.start < avail.end && args.end > avail.start));

        let overlapEvent = events.some(event => args.start < event.end && args.end > event.start);
        if (overlapEvent) {
          const overlappingEvent = events.find(event => args.start < event.end && args.end > event.start);
          console.log(overlappingEvent.lesson.numberOfStudents);
          if (overlappingEvent.lesson.numberOfStudents > 1) {
            console.log("you can be taught like a group");
          }
          else {
            console.log("you cant book this, no capacity")
          }
        }

        if (overlapAvail && !overlapEvent) {
          const dateTime = new DayPilot.Date(args.start);
          const start = getTime(dateTime);
          const label = start + "\n" + pb.authStore.model.name;
          setShowModal(!showModal);
          setArgs(args);
        }
        else {
          console.log("you cant create event here");
        }
      }
      else {
        console.log("you cant create event here");
      }
    }
  }

  const [eventData, setEventData] = useState('');

  const handleEventClick = async (args) => {
    console.log("click event");

    const isEditable = args.e.data.editable;
    
    if (isEditable) {
      const clickedEvent = await pb.collection('events').getOne(args.e.data.id, {expand: 'lesson, creator, participants'})
      setShowOnClickModal(true);
      setEventData(clickedEvent);
    }
  }

  const handleEventMove = async (args) => {
    const sameDayAvail = availability.filter((avail) => {
      return avail.start.getDatePart() === args.start.getDatePart();
    })
    if (sameDayAvail.length > 0) {
      let overlap = sameDayAvail.some(avail => args.start < avail.end && args.end > avail.start);
      if (overlap) {
        console.log("moving event")
      }
      else {
        console.log("you cant move event here");
      }
    }
    else {
      console.log("you cant move event here");
    }
    console.log("moving event");
    const e = args.e;
    await pb.collection('events').update(e.data.id, {
      text: e.text,
      start: e.data.start,
      end: e.data.end,
      backColor: e.data.backColor
    })
  }

  const handleEventDelete = async (args) => {
    const dp = getCalendar();
    const eventId = args.e.data.id;

    try {
      await pb.collection("events").delete(eventId);
      dp.events.remove(eventId);
      setEvents((events) => events.filter((event) => event.id !== eventId));
    } catch (error) {
    return { error: error.message };
    }
    console.log("deleting event");
  }

  const toggleShow = () => {
    setShowModal(!showModal);
  } 
  
  console.log(events);
  const allEvents = availability ? ([...events, ...availability]) : ([...events]);

  if (localStorage.getItem('selectedDate') === null) {
    localStorage.setItem('selectedDate', new DayPilot.Date());
  }
    
  return (
      <div style={styles.wrap} className="cal">
        <div style={styles.left}>
          <DayPilotNavigator
            selectMode={"Week"}
            weekStarts = {1}
            locale = 'cs-CZ'
            showMonths={2}
            skipMonths={2}
            startDate={new DayPilot.Date(localStorage.getItem('selectedDate'))}
            onTimeRangeSelected={ args => {
              localStorage.setItem('selectedDate', args.start.value);
              getCalendar().update({
                startDate: localStorage.getItem('selectedDate')
              });
            }}
          />
        </div>
        <div style={styles.main}>
          <DayPilotCalendar
            events={allEvents}
            ref={calendarRef}
            viewType = {"Week"}
            weekStarts = {1}
            locale = 'cs-CZ'
            headerDateFormat = {'dddd d.M.yyyy'}
            durationBarVisible = {false}
            timeRangeSelectedHandling = {"Hold"}
            eventDeleteHandling = {"Update"}
            onTimeRangeSelected={handleTimeRangeSelected}
            onEventDelete={handleEventDelete}
            onEventClicked={handleEventClick}
            onEventMove={handleEventMove}
          />
        </div>
        {showModal && <CreateEvent showModal={showModal} setShowModal={setShowModal} toggleShow={toggleShow} newEvent={newEvent} data={data} overlapAvail={overlapAvail} overlapEvents={overlapEvents}></CreateEvent>}
        {showOnClickModal && <ViewEvent showModal={showOnClickModal} toggleShow={setShowOnClickModal} eventData={eventData}></ViewEvent>}
      </div>
  );
}
