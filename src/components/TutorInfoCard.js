import { Card, Button, Badge, Tooltip } from 'flowbite-react';
import { VideoCameraIcon } from "@heroicons/react/24/outline";
import React, { useState, useEffect } from "react";
import pb from '../lib/pocketbase';

export default function TutorInfoCard(record) {
    const tutor = record.tutor;
    const lessons = tutor.expand.lessons.map(lesson => lesson.name);
    const prices = tutor.expand.lessons.map(lesson => lesson.price);
    const lowestPrice = Math.min(...prices);
    const [reviews, setReviews] = useState([]);
    console.log(tutor.id);

    return(
        <Card key={tutor.id} className="col-span-1 flex flex-col">
            <div className="flex flex-col">
                <div className="flex flex-row">
                    <img
                        className="mb-3 h-24 w-24 rounded-full shadow-lg"
                        src={pb.files.getUrl(tutor, tutor.avatar, {'thumb': '150x150'})}
                        alt="Profilový obrázek"
                    />
                    
                    <div className="flex-col pl-6">
                        <div className="flex flex-row items-center gap-2">
                            <h5 className="text-2xl font-bold tracking-tight text-gray-900 dark:text-white">
                                {tutor.name}
                            </h5>
                            {tutor.tutoringOnline && 
                                (
                                    <Tooltip content="Doučuje online" placement="right">
                                        <Badge size="sm" color="success">
                                            <div className="flex gap-1">
                                                <VideoCameraIcon className="h-5 w-5"/>
                                            </div>
                                        </Badge>
                                    </Tooltip>
                                )
                            }
                        </div>

                        <div className="flex flex-wrap gap-2 my-2">
                        {tutor.expand.lessons.map((lesson) => {
                            return(
                                <Badge key={lesson.id} size="sm" color="info">
                                    {lesson.name}
                                </Badge>
                                )
                            })}
                        </div>

                </div>
                </div>
                <span className="text-sm text-gray-500 dark:text-gray-700">
                    {tutor.aboutMe}
                </span>
            </div>

            <div className="mt-auto">
                <div className="text-right text-lg font-bold text-gray-700 mb-2">od {lowestPrice} Kč/hod</div>
                <Button href={`/calendar/${tutor.id}`} disabled={!pb.authStore.isValid}>
                    Vybrat termín
                </Button>
            </div>
        </Card>
    )
}