import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import pb from "../../lib/pocketbase";
import { useNavigate } from 'react-router-dom';
import { Button, Card, Label, TextInput } from "flowbite-react";

export default function Lesson() {

    const {id} = useParams();
    const [name, setName] = useState("");
    const [price, setPrice] = useState("");
    const [duration, setDuration] = useState("");
    const [numOfStudents, setNumOfStudents] = useState("");
    const nav = useNavigate();

    useEffect(() => {
        const getLesson = async() => {
            return await pb.collection('lessons').getOne(id);
        }
        getLesson().then((lesson) => {
            setName(lesson.name);
            setPrice(lesson.price);
            setDuration(lesson.duration);
            setNumOfStudents(lesson.numberOfStudents);
        });
    }, [id])

    const handleNameChange = (event) => {
        setName(event.target.value);
    };

    const handlePriceChange = (event) => {
        setPrice(event.target.value);
    };

    const handleDurationChange = (event) => {
        setDuration(event.target.value);
        console.log(duration);
    };

    const handleNumOfStudentsChange = (event) => {
        setNumOfStudents(event.target.value);
    };
    
    const submit = async (event) => {
        event.preventDefault();
        await pb.collection('lessons').update(id, {
            name: name,
            price: price,
            numberOfStudents: numOfStudents,
            duration: duration
        })
        console.log(id, name, price, numOfStudents, duration);
        nav("/lessons");
    };

    return(
        <Card className="w-full lg:w-1/3 mx-auto">
            <div className="mb-2 text-2xl font-bold tracking-tight text-gray-900 dark:text-white">Upravení předmětu</div>
            <form onSubmit={submit} className="flex flex-col gap-4">
                <div>
                    <div className="mb-2 block">
                    <Label htmlFor="name" value="Název"/>
                    </div>
                    <TextInput value={name} onChange={handleNameChange} id="name" type="text" required={true}/>
                </div>
                <div>
                    <div className="mb-2 block">
                    <Label htmlFor="duration" value="Trvání"/>
                    </div>
                    <TextInput value={duration} onChange={handleDurationChange} id="duration" type="text" required={true}/>
                </div>
                <div>
                    <div className="mb-2 block">
                    <Label htmlFor="numOfStudents" value="Počet studentů"/>
                    </div>
                    <TextInput value={numOfStudents} onChange={handleNumOfStudentsChange} id="numOfStudents" type="text" required={true}/>
                </div>
                <div>
                    <div className="mb-2 block">
                    <Label htmlFor="price" value="Cena"/>
                    </div>
                    <TextInput value={price} onChange={handlePriceChange} id="price" type="text" required={true}/>
                </div>
                
                <div className="flex justify-between mt-2">
                        <Button href="/lessons">Zpět</Button>
                    <Button type="submit">Potvrdit</Button>
                </div>
            </form>
        </Card>
    )
}
