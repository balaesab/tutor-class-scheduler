import React, { useState, useEffect } from "react";
import { Link } from 'react-router-dom';
import pb from "../../lib/pocketbase";
import { Button, Card, Label } from "flowbite-react";
import { PlusIcon } from "@heroicons/react/24/outline";

export default function Lessons() {
    const [lessons, setLessons] = useState([]);
    const role = pb.authStore.model.role;

    useEffect(() => {
        const getLessons = async() => {
            if (role === "tutor") {
                return await pb.collection('lessons').getFullList({filter: `creator="${pb.authStore.model.id}"`, expand: 'creator'});
            }
            return await pb.collection('lessons').getFullList({expand: 'creator'});
        }
        getLessons().then((data) => {
            setLessons(data);
            console.log(data);
        });
    }, [role]);
        
    const handleLessonRemoval = async (lesson) => {
        await pb.collection("lessons").delete(lesson.id);
        const tempLessons = lessons.filter(l => l.id !== lesson.id);
        console.log(tempLessons);
        setLessons(tempLessons);
    };

    return(
        <div>
            {role === "tutor" &&
                <div className="flex justify-end mb-5">
                    {<Button href="/createLesson">Vytvořit hodinu
                        <PlusIcon className="ml-1 h-6 w-6 text-white-500" />
                    </Button>}
                </div>
            }
            <div className="flex flex-wrap -m-4">
                {lessons.map((lesson)=> {
                    return(
                        <div className="p-4 sm:w-1/3 lg:w-1/3" key={lesson.id}>
                            <Card className="card">
                                <div className="flex flex-col space-y-1">
                                    <div className="mb-2">
                                        <div className="mb-2 text-2xl font-bold tracking-tight text-gray-900 dark:text-white">{lesson.name}</div>
                                        <div className="space-y-2">
                                            <div>
                                                <Label htmlFor="duration">Trvání</Label>
                                                <div id="duration" className="">{lesson.duration} hod</div>
                                            </div>

                                            <div>
                                                <Label htmlFor="numOfStudents">Počet studentů</Label>
                                                <div id="numOfStudents" className="">{lesson.numberOfStudents}</div>
                                            </div>

                                            <div>
                                                <Label htmlFor="price">Cena</Label>
                                                <div id="price" className="price">{lesson.price}Kč</div>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="flex flex-row justify-between">
                                        <Link to={`/lessons/${lesson.id}`}>
                                            <Button>Upravit</Button>
                                        </Link>
                                        <Button onClick={() => handleLessonRemoval(lesson)}>Odstranit</Button>
                                    </div>
                                </div>

                            </Card>
                        </div>
                    )}
                )}
            </div>
        </div>
    );
}