import React, { useState, useEffect } from "react";
import pb from '../../lib/pocketbase';
import { useParams } from "react-router-dom";
import Payment from "./Payment";

export default function Payments() {
    const [payments, setPayments] = useState([]);
    const [user, setUser] = useState("");
    const {id} = useParams();
    const role = pb.authStore.model.role;

    useEffect(() => {
        const getPayments = async() => {
            if (role === "student") {
                if (id) {
                    return await pb.collection('events').getFullList({expand:'creator, lesson, payment, payment.tutor, payment.student, participants', filter: `payment.tutor.id?="${id}" && payment.student.id?="${pb.authStore.model.id}"`});
                }
                return await pb.collection('events').getFullList({expand: 'creator, lesson, payment, payment.tutor, payment.student, participants', filter: `participants.id?="${pb.authStore.model.id}"`});
            }
            else if (role === "tutor") {
                if (id) {
                    return await pb.collection('events').getFullList({
                        expand: 'creator, lesson, payment, payment.tutor, payment.student, participants',
                        filter: `payment.student.id="${id}" && payment.tutor.id="${pb.authStore.model.id}"`
                    });
                }
                return await pb.collection('events').getFullList({expand: 'creator, lesson, payment, payment.tutor, payment.student, participants', filter: `payment.tutor.id="${pb.authStore.model.id}"`});
            }
        }
        getPayments().then((data) => {
            setPayments(data);
            console.log(data);
        });

        const getUser = async() => {
            return await pb.collection('users').getOne(id);
        }

        if (id) {
            getUser().then((user) => {
                console.log(user);
                setUser(user);
            });
        }
    }, [id]);

    console.log(payments)
    console.log(pb.authStore.model.id)
    return(
        <div>
            {id ? (role==="tutor" ?  (<h3 className="mb-4 font-medium">Platby od {user.name}</h3>) : (<h3 className="mb-4 font-medium">Platby pro {user.name}</h3>)) : (<></>)}
            <div className="flex flex-col gap-4">
                {
                    Object.keys(payments).length === 0 ? (<>Žádné platby</>) : (
                        payments.map((event)=> {
                            return(
                                <Payment event={event} key={event.id}></Payment>
                            )}
                        )
                    )
                }
            </div>
        </div>
    );
}