import React, { useState } from "react";
import pb from '../../lib/pocketbase';
import { formatDate } from '../../utils/format.js';
import {DayPilot} from "@daypilot/daypilot-lite-react";
import { Card, Button, Avatar } from 'flowbite-react';

export default function Payment(record) {
    const [showPayBtn, setShowPayBtn] = useState(Object.keys(record.event.payment).length === 0);
    const role = pb.authStore.model.role;
    const payments = record.event.expand.payment;
    const lesson = record.event.expand.lesson;
    const participants = record.event.expand.participants;
    const haventPayed = [];
    const dateStart = new DayPilot.Date(record.event.start);
    const date = formatDate(dateStart);

    const handlePayment = async(event) => {
        const response = await pb.collection('payments').create({
            event: event.id,
            student: pb.authStore.model.id,
            tutor: event.creator,
            approved: false
        })
        const createdPaymentId = response.id;
        console.log(createdPaymentId);
        await pb.collection('events').update(event.id, {payment: createdPaymentId})
        setShowPayBtn(false);
    }

    participants.forEach(participant => {
        let hasPayment = false;
        if (payments) {
            payments.forEach(payment => {
                if (payment.student === participant.id) {
                    hasPayment = true;
                }
            })
            if (!hasPayment) {
                haventPayed.push(participant);
            }
        } else {
            haventPayed.push(participant);
        }
    })

    if (role === "tutor") {
        return(
            <>
                <Card>
                <div className="flex flex-wrap  items-center">
                    <div className="flex-1">
                        <div className="text-md font-bold tracking-tight text-gray-900">{lesson.name}</div>
                    </div>
                    <div className="flex-1">
                        <div className="sub-title">{date}</div>
                    </div>
                    <div className="flex-1">
                        {
                            participants && participants.map(p => {
                                return(
                                    <div key={p.id}>{p.name}</div>
                                )
                            })
                        }
                    </div>
                    <div className="flex-1">
                        {
                            participants.length !== 1 && haventPayed.length >= 1 && (
                                <>
                                    <div className="sub-title">Nezaplatil: </div>
                                    {
                                        haventPayed.map(p => {
                                            return(
                                                <>
                                                    <div key={p.id} className="sub-title">{p.name}</div>
                                                </>
                                            )
                                        })
                                    }
                                </>
                            )
                        }
                    </div>
                    <div className="flex-1">
                        <div className="sub-title">Zaplaceno {payments ? (payments.length) : ('0')}/{participants.length}</div>
                    </div>
                </div>
                </Card>
            </>
        )
    }
    else if (role === "student") {
        return(
            <Card>
                <div className="flex flex-wrap  items-center">
                    <div className="flex gap-4 items-center flex-1">
                        <Avatar alt="User settings" img={pb.files.getUrl(record.event.expand.creator, record.event.expand.creator.avatar, {'thumb': '150x150'})} rounded={true}/>
                        <div>{record.event.expand.creator.name}</div>
                    </div>
                    <div className="flex-1">{record.event.expand.lesson.name}</div>
                    <div className="flex-1">{date}</div>
                    {
                        showPayBtn ? (<Button className="flex-1" onClick={() => handlePayment(record.event)}>Zaplatit</Button>) :
                        (<Button className="flex-1" gradientDuoTone="greenToBlue" outline={true} disabled={true}>
                            Zaplaceno
                        </Button>)
                    }
                </div>
            </Card>
        )
    }
}