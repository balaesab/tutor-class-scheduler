import React, { useState, useEffect } from "react";
import pb from "../../lib/pocketbase";
import { Link } from 'react-router-dom';
import { useParams } from "react-router-dom";
import { Card, Button, Avatar, Label, TextInput } from 'flowbite-react';
import { PencilIcon } from "@heroicons/react/24/outline";

export default function Profile() {
    const [profile, setProfile] = useState('');
    const {id} = useParams();
    const role = pb.authStore.model.role;
    const [reviews, setReviews] = useState('');

    useEffect(() => {
        const getProfile = async() => {
            if (id) {
                return await pb.collection('users').getOne(id);
            }
            return await pb.collection('users').getOne(pb.authStore.model.id);
        }
        getProfile().then((data) => {
            setProfile(data);
            console.log(data);
        });
    }, [id]);

    useEffect(() => {
        const getReviews= async() => {
            if (id) {
                return await pb.collection('reviews').getFullList({filter: `tutor="${id}"`, expand: 'student'});
            }
            return null;
        }
        getReviews().then((data) => {
            setReviews(data);
            console.log(data);
        });
    }, [id]);

    
        return(
            <>
            <Card className="w-full lg:w-1/2 mx-auto">
            <div className="flex flex-col gap-4">
                <div><Avatar size="lg" alt="User settings" img={pb.files.getUrl(profile, profile.avatar, {'thumb': '150x150'})} rounded={true}/></div>
                <div>
                    <div className="mb-2 block">
                    <Label htmlFor="name" value="Jméno"/>
                    </div>
                    <TextInput className="pointer-events-none" placeholder={profile.name} id="name"type="text"/>
                </div>
                <div>
                    <div className="mb-2 block">
                    <Label htmlFor="email1" value="Email"/>
                    </div>
                    <TextInput className="pointer-events-none" placeholder={profile.email} id="email1"type="email"/>
                </div>
                {
                    role === "tutor" && (
                        <div>
                            <div className="mb-2 block">
                            <Label htmlFor="aboutMe" value="O mně"/>
                            </div>
                            <textarea id="aboutMe" rows="8" placeholder={profile.aboutMe} className="pointer-events-none block p-2.5 w-full text-sm text-gray-900 bg-gray-50 rounded-lg border border-gray-300 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"  required={true}></textarea>
                        </div>
                    )
                }
            </div>
            <div className="flex justify-end mt-2">
                <Link to="/editProfile">
                    <Button>
                        <div className="flex gap-2 items-center">
                            <div>Upravit</div>
                            <PencilIcon className="h-5 w-5 text-white-500" />
                        </div>
                    </Button>
                </Link>
            </div>
        </Card>

        {id && (
            <div className="list">
                {reviews && reviews.map(review => {
                    return(
                        <div className="card">
                            {console.log(reviews)}
                            <div className="card-title">{review.expand.student.name}</div>
                            <div>{review.comment}</div>
                            <div>{review.rating}/5</div>
                        </div>
                    )
                })}
            </div>
        )}
        </>
    )
}