import React, { useState, useEffect } from "react";
import pb from "../../lib/pocketbase";
import { useNavigate, Link } from 'react-router-dom';
import { Label, TextInput, Avatar, Button, Card } from 'flowbite-react';

export default function EditProfile() {
    const [profile, setProfile] = useState('');
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [avatar, setAvatar] = useState(null);
    const [aboutMe, setAboutMe] = useState('');
    const role = pb.authStore.model.role;
    const nav = useNavigate();
    
    useEffect(() => {
        const getProfile = async() => {
            return await pb.collection('users').getOne(pb.authStore.model.id);
        }
        getProfile().then((profile) => {
            setProfile(profile);
            setName(profile.name);
            setEmail(profile.email);
            setAvatar(profile.avatar);
            setAboutMe(profile.aboutMe);
        });
    }, [])

    const handleNameChange = (event) => {
        setName(event.target.value);
    }

    const handleEmailChange = (event) => {
        setEmail(event.target.value);
    }
    
    const handleAboutMeChange = (event) => {
        setAboutMe(event.target.value);
    }
    
    const submit = async(event) => {
        event.preventDefault();
        console.log(event.target.name);

        await pb.collection('users').update(pb.authStore.model.id, {
            name: name,
            email: email,
            aboutMe: aboutMe
        })
        nav("/profile");
    }

    return(
        <Card className="w-full lg:w-1/2 mx-auto">
            <form onSubmit={submit} className="flex flex-col gap-4">
                <div><Avatar size="lg" alt="User settings" img={pb.files.getUrl(profile, profile.avatar, {'thumb': '150x150'})} rounded={true}/></div>
                <div>
                    <div className="mb-2 block">
                    <Label htmlFor="name" value="Jméno"/>
                    </div>
                    <TextInput value={name} onChange={handleNameChange} id="name"type="text" placeholder="Jméno Příjmení" required={true}/>
                </div>
                <div>
                    <div className="mb-2 block">
                    <Label htmlFor="email1" value="Email"/>
                    </div>
                    <TextInput value={email} onChange={handleEmailChange} id="email1"type="email" placeholder="email@example.com" required={true}/>
                </div>
                {
                    role === "tutor" && (
                        <div>
                            <div className="mb-2 block">
                            <Label htmlFor="aboutMe" value="O mně"/>
                            </div>
                            <textarea id="aboutMe" rows="8" value={aboutMe} onChange={handleAboutMeChange} placeholder={profile.aboutMe} className="block p-2.5 w-full text-sm text-gray-900 bg-gray-50 rounded-lg border border-gray-300 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"  required={true}></textarea>
                        </div>
                    )
                }
                <div className="flex justify-between mt-2">
                    <Link to="/profile">
                        <Button>Zpět</Button>
                    </Link>    
                    <Button type="submit">Potvrdit</Button>
                </div>
            </form>
        </Card>
    )
}