import React, {useState} from 'react';
import { useEffect } from 'react';
import { useParams } from "react-router-dom";
import pb from '../../lib/pocketbase';
import MyModal from '../MyModal';
import { Button, Label } from "flowbite-react";
import { getTime, getDate } from '../../utils/format';
import {DayPilot} from "@daypilot/daypilot-lite-react";

export default function CreateEvent(props) {
    const [showModal, setShowModal] = useState(props.showModal);
    const [lesson, setLesson] = useState('');
    const [lessons, setLessons] = useState([]);
    const [isSelected, setIsSelected] = useState(true);
    const {id} = useParams();
    const [isOverflowing, setIsOverflowing] = useState(false);
    const [start, setStart] = useState('');
    const [end, setEnd] = useState('');
    const [date, setDate] = useState('');
    const [isOverlapping, setIsOverlapping] = useState(false);
    const [parsedLesson, setParsedLesson] = useState('');

    useEffect(() => {
        const getLessons = async() => {
            return await pb.collection('lessons').getFullList({filter: `creator="${id}"`});
        }
        getLessons().then(data => {
            setLessons(data); 
        })
        const date = getDate(new DayPilot.Date(props.data.start.value));
        setDate(date);
        const start = getTime(new DayPilot.Date(props.data.start.value));
        setStart(start);
    }, [])

    const getLessonEnd = (selectedLesson) => {
        const parsedLesson = JSON.parse(selectedLesson);
        const startDayPilotDate = new DayPilot.Date(props.data.start.value);
        return startDayPilotDate.addHours(parsedLesson.duration);
    }

    const checkOverlaps = (end) => {
        const start = props.data.start.value;
        setIsOverlapping(props.overlapEvents.some(event => {
            return start < event.end && end > event.start;
        }));
    }
    
    const handleLessonChange = (e) => {
        const selectedLesson = e.target.value;
        if (selectedLesson) {
            setParsedLesson(JSON.parse(selectedLesson));
        }
        setIsOverflowing(false);
        setIsOverlapping(false);
        if(selectedLesson) {
            setIsSelected(true);
            const end = getLessonEnd(selectedLesson);
            setEnd(getTime(end));
            if (end > props.overlapAvail.end) {
                setIsOverflowing(true);
            }
            checkOverlaps(end);
        }
        else {
            setIsSelected(false);
        }
        setLesson(selectedLesson);
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        if (isSelected && !isOverflowing && !isOverlapping) {
            props.newEvent(lesson);
            setLesson('');
            setIsSelected(false);
        }
    }

    console.log(lessons);
    return(
        <MyModal showModal={showModal} toggleShow={props.toggleShow}>
            <div className="mt-4 w-80">
            <h2 className="text-xl font-bold mb-4">Rezervovat hodinu</h2>
            <form onSubmit={e => handleSubmit(e)}>
            <div className="mb-5">
                <div className="mb-2 block">
                    <Label htmlFor="lesson" value="Předmět"/>
                    <select id="lesson" value={lesson} onChange={handleLessonChange} className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                        <option value="">Vyberte předmět</option>
                        {lessons.map((lesson) => {
                            return(
                                <option key={lesson.id} value={lesson ? JSON.stringify(lesson) : ""}>{lesson.name}</option>
                                )
                            })}
                    </select>
                    {!isSelected && <div className="text-red-500 text-sm">Nezvolen předmět!</div>}
                </div>
                <div className="mb-2 block">
                <Label htmlFor="date" value="Datum"/>
                    <div id="date">{date}</div>
                </div>
                <div className="mb-2 block">
                    <Label htmlFor="duration" value="Trvání"/>
                    <div id="duration" className="flex flex-row">
                        <div>{start}</div>
                        {lesson && <div>&nbsp;-&nbsp;{end}</div>}
                    </div>
                    {isOverflowing && <div className="text-red-500 text-sm">Zvolte dřívější hodinu!</div>}
                    {isOverlapping && <div className="text-red-500 text-sm">Vybraná hodina se kryje s již obsazeným slotem!</div>}
                </div>

                <div className="mb-2 block">
                    <Label htmlFor="price" value="Cena"/>
                        <div id="price">{lesson && parsedLesson.price} Kč</div>
                </div>

                </div>
                <div className="mt-auto">
                    <Button className="w-full" type="submit">Rezervovat</Button>
                </div>
            </form>
            </div>
        </MyModal>
    )
}
