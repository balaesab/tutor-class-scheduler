import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import pb from "../../lib/pocketbase";
import Event from "./Event";
import {DayPilot} from "@daypilot/daypilot-lite-react";

export default function UpcomingEvents() {
    const [events, setEvents] = useState([]);
    const [student, setStudent] = useState("");
    const {id} = useParams();
    
    const getEvents = async() => {
        const dateTime = new DayPilot.Date().toString().split('T')[0] + " " + new DayPilot.Date().toString().split('T')[1];
        if (pb.authStore.model.role === "tutor") {
            if (id) {
                return await pb.collection('events').getFullList({expand:'participants, lesson', filter: `participants.id?="${id}" && creator.id="${pb.authStore.model.id}"`});
            }
            return await pb.collection('events').getFullList({sort: 'created', expand: 'creator, participants, lesson', filter: `creator.id?="${pb.authStore.model.id}"`});
        }
        else if (pb.authStore.model.role === "student") {
            if (id) {
                return await pb.collection('events').getFullList({expand:'creator, participants, lesson', filter: `creator.id="${id}" && participants.id?="${pb.authStore.model.id}"`});
            }
            return await pb.collection('events').getFullList({sort: 'created', expand: 'creator, participants, lesson', filter: `participants.id="${pb.authStore.model.id}"`});
        }
    }

    const getStudent = async() => {
        return await pb.collection('users').getOne(id);
    }
    
    useEffect(() => {
        getEvents().then((data) => {
            const eventsWithParticipants = [];
            data.forEach(event => {
                if (typeof event.expand.participants !== 'undefined') {
                    eventsWithParticipants.push(event);
                }
            })
            setEvents(eventsWithParticipants);
            console.log(eventsWithParticipants);            
        });

        if (id) {
            getStudent().then((student) => {
                console.log(student);
                setStudent(student);
            });
        }
    }, [id]);

    const handleEventRemoval = async(id) => {
        await pb.collection("events").delete(id);
        console.log("delete");
        const tempEvents = events.filter(e => e.id !== id);
        console.log(tempEvents);
        setEvents(tempEvents);
    }
    
    return(
        <div>
            {id && <h3 className="mb-4 font-medium">Doučování s {student.name}</h3>}

            {Object.keys(events).length === 0  && <div className="text-center mt-20">Nemáte žádné nadcházející události</div>}
            {Object.keys(events).length !== 0 && 
                <div className="grid grid-cols-1 gap-6 md:grid-cols-2 lg:grid-cols-3">
                    {
                    events.map((event)=> {
                        return(
                            <Event event={event} key={event.id} onDeleteEvent={() => handleEventRemoval(event.id)}></Event>
                        )
                    })
                    }
                </div>
            }
        </div>
    );
    
}