import pb from "../../lib/pocketbase";
import { formatDate } from '../../utils/format.js';
import {DayPilot} from "@daypilot/daypilot-lite-react";
import { Card, Button, Avatar } from 'flowbite-react';

export default function Event({event, onDeleteEvent}) {
    const dateStart = new DayPilot.Date(event.start);
    const start = formatDate(dateStart);

    let arr = [];
    for (let i = 0; i < event.expand.participants.length; i++) {
        arr.push(event.expand.participants[i].name);
    }
    const str = arr.join(', ');

    return(
        <Card key={event.id} className="col-span-1 flex flex-col">
            <div className="flex flex-row">
                {pb.authStore.model.role==="tutor" ? (
                    <></>
                ) : (
                    <img
                        className="mb-3 h-24 w-24 rounded-full shadow-lg"
                        src={pb.files.getUrl(event.expand.creator, event.expand.creator.avatar, {'thumb': '150x150'})}
                        alt="Profilový obrázek"
                    />
                )}
                <div className={`flex flex-col ${pb.authStore.model.role === "tutor" ? "" : "pl-6"}`}>
                    <div className="text-xl font-bold tracking-tight text-gray-900 dark:text-white">{event.expand.lesson.name}</div>
                    <div className="text-lg font-medium">{start}</div>
                    {pb.authStore.model.role === "tutor" ? <div className="text-lg">Student: {str}</div> : <div className="text-lg font-medium">Učitel: {event.expand.creator.name}</div>}
                    <div className="text-md text-gray-500">{event.expand.lesson.duration} hod</div>
                </div>
            </div>
            <div></div>
            <div className="text-right text-lg font-bold text-gray-700">{event.expand.lesson.price}Kč</div>
            <div className="flex justify-end">
                <Button className="card-btn" onClick={onDeleteEvent}>Zrušit</Button>
            </div>
        </Card>
    )
}
