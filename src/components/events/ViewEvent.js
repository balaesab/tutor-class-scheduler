import { useState } from "react";
import MyModal from "../MyModal";
import { Button, Label } from "flowbite-react";
import { getDate, getTime } from "../../utils/format";
import {DayPilot} from "@daypilot/daypilot-lite-react";

export default function ViewEvent(props) {
    const [showModal, setShowModal] = useState(props.showModal);
    const event = props.eventData;
    const start = getTime(new DayPilot.Date(event.start));
    const end = getTime(new DayPilot.Date(event.end));
    const date = getDate(new DayPilot.Date(event.start));
    const participantsNum = event.expand.participants.length;
    const capacity = event.expand.lesson.numberOfStudents;

    return(
        <MyModal showModal={showModal} toggleShow={props.toggleShow}>
            <div className="mt-4 w-80">
            <h2 className="text-xl font-bold mb-4">Rezervovaná hodina</h2>
            </div>
            <div className="mb-2 block">
                <Label htmlFor="date" value="Datum"/>
                <div id="date">{date}</div>
            </div>
            <div className="mb-2 block">
                <Label htmlFor="lesson" value="Předmět"/>
                <div id="lesson">{event.expand.lesson.name}</div>
            </div>
            <div className="mb-2 block">
                <Label htmlFor="duration" value="Trvání"/>
                <div className="flex flex-row">
                    <div id="duration">{start}</div>
                    <div id="duration">&nbsp;-&nbsp;{end}</div>
                </div>
            </div>
            <div className="mb-2 block">
                <Label htmlFor="studentsNum" value="Počet studentů"/>
                <div id="studentsNum">{`${participantsNum}/${capacity}`}</div>
            </div>
            {participantsNum < capacity && (
                <Button className="w-full">Rezervovat také</Button>
            )}
        </MyModal>
    )
}