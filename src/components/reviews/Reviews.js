import React, {useState, useEffect} from "react";
import pb from '../../lib/pocketbase';
import Review from "./Review";
import CreateReviewModal from "./CreateReviewModal";
import { Button } from "flowbite-react";

export default function Reviews() {
    const [showModal, setShowModal] = useState(false);
    const [reviews, setReviews] = useState([]);
    const role = pb.authStore.model.role;
    
    const getReviews = async() => {
        if (role === "tutor") {
            return await pb.collection('reviews').getFullList({expand: 'student'});
        } else if (role === "student") {
            return await pb.collection('reviews').getFullList({filter: `student="${pb.authStore.model.id}"`, expand: 'tutor'});
        }
    }

    useEffect(() => {
        getReviews().then((data) => {
            setReviews(data);
            console.log(data);
        });
    }, []);

    const toggleShow = () => {
        setShowModal(!showModal);
    } 

    const newReview = (selectedTutor, comment, rating) => {
        const tutor = JSON.parse(selectedTutor);
        toggleShow();
        if (selectedTutor) {
            console.log("creating review");
            const createReview = async() => { 
                    return await pb.collection('reviews').create({
                    student: pb.authStore.model.id,
                    tutor: tutor.id,
                    comment: comment,
                    rating: rating
                });
            }
            createReview().then(() => {
                getReviews().then((data) => {
                    setReviews(data);
                })
            })
            toggleShow();
        }
        else {
            console.log("Not creating review");
        }
    }

    const handleDelete = async(id) => {
        await pb.collection("reviews").delete(id);
        console.log("deleting review");
        const tempEvents = reviews.filter(e => e.id !== id);
        console.log(tempEvents);
        setReviews(tempEvents);
    }

    console.log(showModal);
    return (
        <>
        <div className="flex justify-end mb-5">
            {role === "student" && <Button onClick={toggleShow}>Ohodnotit učitele</Button>}
        </div>
        <div className="flex flex-wrap -m-4">
            {reviews.map((review)=> {
                    return(
                        <Review review={review} key={review.id} onDelete={() => handleDelete(review.id)}/>
                    )
                }
            )}
        </div>
        {showModal && <CreateReviewModal showModal={showModal} toggleShow={toggleShow} newReview={newReview}></CreateReviewModal>}
        </>
    )
}