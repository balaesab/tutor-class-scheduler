import React, {useState} from "react";
import pb from '../../lib/pocketbase';
import { StarIcon, PaperAirplaneIcon } from "@heroicons/react/24/outline";
import { Card, Button, TextInput, Avatar, Tooltip } from 'flowbite-react';

export default function Review({review, onDelete}) {
    const [showInput, setShowInput] = useState(!review.response);
    const [response, setResponse] = useState(review.response);
    const role = pb.authStore.model.role;
    const stars = [];
    
    const handleResponseChange = (event) => {
        setResponse(event.target.value);
    };

    const handleReply = async (event, id) => {
        event.preventDefault();
        await pb.collection('reviews').update(id, {
            response: response
        })
        setShowInput(false);
        console.log(response);
    };

    const getStars= (rating) => {
        const maxStars = 5;
        const filledStars = rating;
        for (let i = 0; i < maxStars; i++) {
            if (i < filledStars) {
                stars.push(<StarIcon key={i} className="h-6 w-6 text-yellow-400 fill-yellow-400" />)
            }
            else {
                stars.push(<StarIcon key={i} className="h-6 w-6 text-gray-300 fill-gray-300" />)
            }
        }
        return (
            <Tooltip content={`${filledStars}/5`} placement="right">
                <div className="flex flex-row">{stars}</div>
            </Tooltip>
        )
    }
    
    return (
        <div className="p-4 sm:w-1/2 lg:w-1/2">
            <Card className="card" key={review.id}>
                {role === "tutor" ? (
                    <>
                    {console.log(review.expand.student.id, review.expand.student.avatar)}
                    <div className="flex flex-row items-center gap-4">
                        {console.log(review)}
                        <Avatar alt="User settings" img={pb.files.getUrl(review.expand.student, review.expand.student.avatar, {'thumb': '150x150'})} rounded={true}/>
                        <div className="text-md font-semibold">{review.expand.student.name}</div>
                    </div>
                    </>
                ) : (
                    <div className="card-subtitle">Komu: {review.expand.tutor.name}</div>
                    )}
                {getStars(review.rating)}
                <div className="flex flex-row items-center"></div>
                
                <div className="relative mr-3 text-sm bg-white py-2 px-4 shadow rounded-xl">
                    <div className="card-title">{review.comment}</div>
                </div>
                    {role === "tutor" && showInput ? (
                            <div className="flex flex-row">
                                <TextInput className="w-full mr-2" type="text" placeholder="Vaše odpověď" value={response} onChange={handleResponseChange}/>
                                <Button onClick={(event) => handleReply(event, review.id)}>
                                    <PaperAirplaneIcon className="h-5 w-5 text-white-500"/>
                                </Button>
                            </div> 
                        ) : (
                        response ? (<div className="relative ml-3 text-sm bg-indigo-100 py-2 px-4 shadow rounded-xl">{response}</div>) : (<div className="relative mr-3 text-sm bg-gray-200 py-2 px-4 shadow rounded-xl border-dashed border-2 border-gray-400">Učitel zatím neodpověděl</div>))
                    }
                    {role==="student" && <Button onClick={onDelete}>Smazat</Button>}
            </Card>
        </div>
    )
}