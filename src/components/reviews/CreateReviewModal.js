import React, {useState, useEffect } from "react";
import pb from '../../lib/pocketbase';
import { Button, TextInput, Label } from "flowbite-react";
import { StarIcon } from "@heroicons/react/24/outline";
import MyModal from "../MyModal";

export default function CreateReviewModal(props) {
    const [showModal, setShowModal] = useState(props.showModal);
    const [tutors, setTutors] = useState([]);
    const [tutor, setTutor] = useState('');
    const [comment, setComment] = useState('');
    const [rating, setRating] = useState('');
    const [isTutorSelected, setIsTutorSelected] = useState(true);
    const [isCommentFilled, setIsCommentFilled] = useState(true);
    const [isRatingFilled, setIsRatingFilled] = useState(true);

    useEffect(() => {
        const getEventsWithStudent = async() => {
            return await pb.collection('events').getFullList({expand: 'creator, participants', filter: `participants.id?="${pb.authStore.model.id}"`});
        }
        getEventsWithStudent().then(data => {
            setTutors(prevTutors => {
              const updatedTutors = [...prevTutors];
              data.forEach(event => {
                if (event.expand.creator && !updatedTutors.some(tutor => tutor.id === event.expand.creator.id)) {
                  updatedTutors.push(event.expand.creator);
                }
              });
              return updatedTutors;
            })
        })
    }, [])

    const toggleShow = () => {
        props.toggleShow();
    }

    const handleTutorChange = (e) => {
        const selectedTutor = e.target.value;
        console.log(selectedTutor)
        if (selectedTutor) {
            setIsTutorSelected(true);
        } else {
            setIsTutorSelected(false);
        }
        setTutor(selectedTutor);
    }
    
    const handleCommentChange = (e) => {
        const filledComment = e.target.value;
        if (filledComment) {
            setIsCommentFilled(true);
        } else {
            setIsCommentFilled(false);
        }
        setComment(filledComment);
        
    }
    
    const handleRatingChange= (filledRating) => {
        if(filledRating!==0) {
            setIsRatingFilled(true);
        } else {
            setIsRatingFilled(false);
        }
        setRating(filledRating);
    }

    const handleSubmit = async (e) => {
        e.preventDefault();

        if (tutor && rating && comment) {
            props.newReview(tutor, comment, rating);
            console.log("creating review");
            setTutor('');
            setComment('');
            setRating('');
            setIsTutorSelected(false);
            setIsCommentFilled(false);
            setIsRatingFilled(false);
        }
        if (!tutor) {
            setIsTutorSelected(false);
        }
        if (!comment) {
            setIsCommentFilled(false);
        }
        if (!rating) {
            setIsRatingFilled(false);
        }
    }

    return (
        <MyModal showModal={showModal} toggleShow={toggleShow}>
            <div className="mt-6 w-80">
            <h2 className="text-xl font-bold mb-4">Ohodnocení učitele</h2>
            <form onSubmit={e => handleSubmit(e)}>
                <div className="mb-5">
                <div className="mb-2 block">
                    <Label htmlFor="tutor" value="Učitel"/>
                    <select id="tutor" value={tutor} onChange={handleTutorChange} className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500">
                        <option value="">Vyberte učitele</option>
                        {tutors.map((tutor) => {
                            return(
                                <option key={tutor.id} value={JSON.stringify(tutor)}>{tutor.name}</option>
                                )
                            })}
                    </select>
                    {!isTutorSelected && <div className="text-red-500 text-sm">Nevybral jste učitele!</div>}
                </div>

                <div className="mb-2 block">
                    <Label htmlFor="name" value="Slovní hodnocení"/>
                    <TextInput type="text" id="comment" value={comment} onChange={handleCommentChange}/>
                    {!isCommentFilled && <div className="text-red-500 text-sm">Nevyplněno slovní hodnocení!</div>}
                </div>
                
                <div className="mb-2 block">
                    <Label htmlFor="rating" value="Počet hvězdiček"/>
                    <div className="flex flex-row">
                        {[1, 2, 3, 4, 5].map((star) => (
                            <span id="rating" key={star} className={`cursor-pointer ${star <= rating ? 'text-yellow-400' : 'text-gray-300'}`} onClick={() => handleRatingChange(star)}>
                            <StarIcon className={`h-6 w-6 ${star <= rating ? 'fill-yellow-400' : 'fill-gray-300'}`} />
                        </span>
                        ))}
                    </div>
                    {!isRatingFilled && <div className="text-red-500 text-sm">Nevyplněn počet hvězdiček!</div>}
                </div>
                </div>
                <div className="mt-auto">
                    <Button className="w-full" type="submit">Odeslat</Button>
                </div>
            </form>
            </div>
        </MyModal>
    )
}