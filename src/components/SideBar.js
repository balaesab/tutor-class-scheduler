import React, { useEffect } from "react";
import {Link, useLocation} from "react-router-dom";
import pb from "../lib/pocketbase";
import { Sidebar } from 'flowbite-react';

export default function SideBar() {
    let role = null;
    if (pb.authStore.isValid) {
        role = pb.authStore.model.role;
    }

    const handleLogout = () => {
        pb.authStore.clear();
    }

    const isCurrentPage = (path) => {
        return window.location.pathname === path;
    }

    if (role === null) {
        return(
            <div>
            <Link to="/auth">
                <button>Přihlásit se</button>
            </Link>
            </div>
        )
    }
    else {
    return(
        <div className="relative ">
            <Sidebar className="border-r border-gray-200">
            <Sidebar.Items>
            <Sidebar.ItemGroup>
                {role === "student" && (
                    <>
                        <Sidebar.Item href="/" className={isCurrentPage("/") ? 'bg-blue-500 text-white hover:bg-blue-500' : ''}>
                        Doučování
                        </Sidebar.Item>
                        <Sidebar.Item href="/calendar" className={isCurrentPage("/calendar") ? 'bg-blue-500 text-white hover:bg-blue-500' : ''}>
                        Kalendář
                        </Sidebar.Item>
                    </>
                )}
                <Sidebar.Item href="/upcomingEvents" className={isCurrentPage("/upcomingEvents") ? 'bg-blue-500 text-white hover:bg-blue-500' : ''}>
                Nadcházející události
                </Sidebar.Item>
                {role === "tutor" && (
                    <>
                        <Sidebar.Item href="/lessons" className={isCurrentPage("/lessons") ? 'bg-blue-500 text-white hover:bg-blue-500' : ''}>
                        Předměty
                        </Sidebar.Item>
                        <Sidebar.Collapse label="Kalendář" open={isCurrentPage("/calendar") || isCurrentPage("/availability") ? (true) : (false)}>
                            <Sidebar.Item href="/calendar" className={isCurrentPage("/calendar") ? 'bg-blue-500 text-white hover:bg-blue-500' : ''}>
                            Rezervované hodiny
                            </Sidebar.Item>
                            <Sidebar.Item href="/availability" className={isCurrentPage("/availability") ? 'bg-blue-500 text-white hover:bg-blue-500' : ''}>
                            Nastavení dostupnosti
                            </Sidebar.Item>
                        </Sidebar.Collapse>
                    </>
                )}
                {role === "tutor" ? (
                    <Sidebar.Item href="/students" className={isCurrentPage("/students") ? 'bg-blue-500 text-white hover:bg-blue-500' : ''}>
                        Studenti
                    </Sidebar.Item>
                ) : (
                    <Sidebar.Item href="/tutors" className={isCurrentPage("/tutors") ? 'bg-blue-500 text-white hover:bg-blue-500' : ''}>
                        Učitelé
                    </Sidebar.Item>
                )}
                <Sidebar.Item href="/payments" className={isCurrentPage("/payments") ? 'bg-blue-500 text-white hover:bg-blue-500' : ''}>
                Platby
                </Sidebar.Item>
                <Sidebar.Item href="/reviews" className={isCurrentPage("/reviews") ? 'bg-blue-500 text-white hover:bg-blue-500' : ''}>
                Hodnocení
                </Sidebar.Item>
                <Sidebar.Item href="/profile" className={isCurrentPage("/profile") ? 'bg-blue-500 text-white hover:bg-blue-500' : ''}>
                Profil
                </Sidebar.Item>
            </Sidebar.ItemGroup>
            <Sidebar.ItemGroup>
                <Sidebar.Item href="/" onClick={handleLogout} >
                    Odhlásit se
                </Sidebar.Item>
            </Sidebar.ItemGroup>
            </Sidebar.Items>
        </Sidebar>
        </div>
    );
    }
}