import React, { useState } from "react";
import pb from "../lib/pocketbase";
import { useNavigate } from "react-router-dom";
import { Label, TextInput, Checkbox, Button, Spinner, Card } from 'flowbite-react';

export default function Auth() {
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [isLoading, setLoading] = useState(false);
    const isLoggedIn = pb.authStore.isValid;
    const nav = useNavigate();

    const handleEmailChange = (event) => {
        setEmail(event.target.value);
    };
    
    const handlePasswordChange = (event) => {
        setPassword(event.target.value);
    };

    const handleSubmit = async (event) => {
        setLoading(true);
        try {
            event.preventDefault();
            console.log(email);
            await pb.collection("users").authWithPassword(email, password);
            nav('/upcomingEvents');
        } catch(e) {
            console.log(e);
        }
        setLoading(false);
    };

    return (
        <>
        <div className="w-full lg:w-1/3 mx-auto">

            <div className="text-2xl font-bold my-4">Přihlášení</div>
            <Card>
                <form onSubmit={handleSubmit} className="flex flex-col gap-4">
                    <div>
                        <div className="mb-2 block">
                        <Label htmlFor="email1" value={"Email"}/>
                        </div>
                        <TextInput id="email1" type="email" placeholder="email@example.com" required={true} value={email} onChange={handleEmailChange}/>
                    </div>
                    <div>
                        <div className="mb-2 block">
                        <Label htmlFor="password1" value="Heslo"/>
                        </div>
                        <TextInput id="password1" type="password" required={true} value={password} onChange={handlePasswordChange}/>
                    </div>
                    <div className="flex items-center gap-2">
                        <Checkbox id="remember" />
                        <Label htmlFor="remember">Zapamatuj si mě</Label>
                    </div>
                    <Button type="submit">
                        {isLoading ? (
                            <>
                                <Spinner aria-label="Spinner button example"/>
                                <span className="pl-3">
                                    Loading...
                                </span>
                            </>
                            ) : (
                                <span className="pl-3">
                                    Přihlásit se
                                </span>
                            )
                        }
                    </Button>
                </form>
            </Card>
        </div>
        </>
    )
}