import React, { useState, useEffect } from "react";
import { Navbar, Dropdown, Avatar, Button } from 'flowbite-react';
import pb from "../lib/pocketbase";
import { useNavigate } from 'react-router';
import { CalendarDaysIcon } from "@heroicons/react/24/outline";

export default function NavBar() {
    const [profile, setProfile] = useState('');
    const [currentPage, setCurrentPage] = useState('');
    
    const handleLogout = () => {
        pb.authStore.clear();
    }

    useEffect(() => {
        setCurrentPage(window.location.pathname);
    }, [])

    useEffect(() => {
        if (pb.authStore.isValid) {
            const getProfile = async() => {
                return await pb.collection('users').getOne(pb.authStore.model.id);
            }
            getProfile().then((data) => {
                setProfile(data);
                console.log(data);
            });
        }
        }, []);

    return(
        <Navbar
        fluid={true}
        rounded={true}
        className="border-b bprder-gray-300"
        >
        <Navbar.Brand href="/">
            <CalendarDaysIcon className="h-9 w-9 mx-2 text-blue-500" />
            <span className="self-center whitespace-nowrap text-3xl font-semibold text-blue-500 dark:text-white">
            DOUČTO
            </span>
        </Navbar.Brand>
        <div className="flex md:order-2 mx-6">
            {pb.authStore.isValid ? 
                (
                <Dropdown
                arrowIcon={false}
                inline={true}
                label={
                    <div className="self-center whitespace-nowrap text-base font-semibold flex items-center gap-4">
                        <div>{profile.name}</div>
                        {profile && (<Avatar alt="User settings" img={pb.files.getUrl(profile, profile.avatar, {'thumb': '150x150'})} rounded={true}/>)}
                    </div>}
                >
                <Dropdown.Header>
                    <span className="block text-sm">
                    {profile.name}
                    </span>
                    <span className="block truncate text-sm font-medium">
                    {profile.email}
                    </span>
                </Dropdown.Header>
                    <Dropdown.Item>
                        Profil
                    </Dropdown.Item>
                <Dropdown.Item>
                    Nastavení
                </Dropdown.Item>
                <Dropdown.Divider/>
                <Dropdown.Item href="/" onClick={handleLogout}>
                    Odhlásit se
                </Dropdown.Item>
                </Dropdown>
                ) : (currentPage !== "/auth" && <Button href="/auth">Přihlásit se</Button>
                )
            }
        </div>
        </Navbar>
    );
}