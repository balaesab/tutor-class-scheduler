import React, { useState, useEffect } from "react";
import { Link } from 'react-router-dom';
import pb from '../lib/pocketbase';
import TutorInfoCard from "./TutorInfoCard";

export default function Homepage() {
    const loggedIn = pb.authStore.model;
    const [tutors, setTutors] = useState([]);

    useEffect(() => {
        const getLessons = async() => {
            return await pb.collection('users').getFullList({filter: 'role="tutor"', expand:'lessons'});
        }
        getLessons().then((data) => {
            setTutors(data);
            console.log(data);
        });
    }, []);

    return(
        <div>
            <div className="grid grid-cols-1 gap-6 md:grid-cols-2 lg:grid-cols-3">
                {tutors && tutors.map((tutor) => {
                    return(
                        <TutorInfoCard key={tutor.id} tutor={tutor}></TutorInfoCard>
                    )
                    }
                )}
            </div>
        </div>
    )
}