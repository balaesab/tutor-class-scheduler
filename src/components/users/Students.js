import React, { useState, useEffect } from "react";
import pb from '../../lib/pocketbase';
import { Link } from 'react-router-dom';
import { useCallback } from "react";
import { Card, Button, Avatar } from 'flowbite-react';

export default function Students() {
    const [students, setStudents] = useState([]);
    
    const getUniqueStudents = useCallback((data) => {
        var participants = [];
        for (let i = 0; i < data.length; i++) {
            if (data[i].expand.participants) {
                for (let j = 0; j < data[i].expand.participants.length; j++) {
                    if (!containsStudent(participants, data[i].expand.participants[j])) {
                        participants.push(data[i].expand.participants[j]);
                    }
                    //console.log(students[i].expand.participants[j]);
                }
            }
        }
        return participants;
    }, []);

    const containsStudent = (array, record) => {
        return array.some(item => JSON.stringify(item) === JSON.stringify(record));
    }

    useEffect(() => {
        const getStudents = async() => {
            return await pb.collection('events').getFullList({expand: 'participants'});
        }
        getStudents().then((data) => {
            setStudents(getUniqueStudents(data));
        });
    }, [getUniqueStudents]);

    console.log(students);
    return(
        <div className="flex flex-col gap-4">
            {Array.from(students).map((student)=> {
                return(
                        <Card key={student.id}>
                            <div className="flex flex-wrap items-center">
                                <div className="flex gap-4 items-center flex-1">
                                    <Avatar alt="User settings" img={pb.files.getUrl(student, student.avatar, {'thumb': '150x150'})} rounded={true}/>
                                    <div className="text-md font-bold tracking-tight text-gray-900">{student.name}</div>
                                </div>
                                <div className="flex-1">{student.email}</div>

                                <div className="flex flex-1 justify-end gap-2">
                                    <Link to={`/payments/student/${student.id}`}>
                                        <Button>Platby</Button>
                                    </Link>
                                    <Link to={`/upcomingEvents/student/${student.id}`}>
                                        <Button>Rezervované hodiny</Button>
                                    </Link>
                                </div>
                            </div>
                        </Card>
                    )
                }
            )}
        </div>
    );
}