import React, { useState, useEffect } from "react";
import pb from '../../lib/pocketbase';
import { Link } from 'react-router-dom';
import { useCallback } from "react";
import { Card, Button, Avatar } from 'flowbite-react';

export default function Tutors() {
    const [tutors, setTutors] = useState([]);
    
    const getUniqueTutors = useCallback((data) => {
        var tutors = [];
        for (let i = 0; i < data.length; i++) {
            if (!containsTutor(tutors, data[i].expand.creator)) {
                tutors.push(data[i].expand.creator);
            }
        }
        return tutors;
    }, []);

    const containsTutor = (array, record) => {
        return array.some(item => JSON.stringify(item) === JSON.stringify(record));
    }

    useEffect(() => {
        const getStudents = async() => {
            return await pb.collection('events').getFullList({expand: 'creator'});
        }
        getStudents().then((data) => {
            setTutors(getUniqueTutors(data));
        });
    }, [getUniqueTutors]);

    console.log(tutors);
    return(
        <div className="flex flex-col gap-4">
            {Array.from(tutors).map((tutor)=> {
                    return(
                        <Card key={tutor.id}>
                            <div className="flex flex-wrap items-center">
                                <div className="flex gap-4 items-center flex-1">
                                    <Avatar alt="User settings" img={pb.files.getUrl(tutor, tutor.avatar, {'thumb': '150x150'})} rounded={true}/>
                                    <div className="text-md font-bold tracking-tight text-gray-900">{tutor.name}</div>
                                </div>
                                <div className="flex-1">{tutor.email}</div>
                                <div className="flex flex-1 justify-end gap-2">
                                    <Link to={`/payments/tutor/${tutor.id}`}>
                                        <Button>Platby</Button>
                                    </Link>
                                    <Link to={`/upcomingEvents/tutor/${tutor.id}`}>
                                        <Button>Rezervované hodiny</Button>
                                    </Link>
                                </div>
                            </div>
                        </Card>
                    )
                }
            )}
        </div>
    );
}