import pb from "./lib/pocketbase";
import {BrowserRouter as Router, Routes, Route} from "react-router-dom"
import React, { useState } from "react";
import Calendar from './components/calendarComponents/Calendar';
import SideBar from './components/SideBar';
import Students from './components/users/Students';
import Tutors from './components/users/Tutors';
import Lessons from './components/lessons/Lessons';
import Lesson from './components/lessons/UpdateLesson';
import Reviews from './components/reviews/Reviews';
import CreateLesson from './components/lessons/CreateLesson';
import Auth from './components/Auth';
import UpcomingEvents from './components/events/UpcomingEvents';
import Payments from './components/payments/Payments';
import Homepage from './components/Homepage';
import Availability from './components/calendarComponents/Availability';
import Profile from './components/profile/Profile';
import EditProfile from './components/profile/EditProfile';
import NavBar from "./components/NavBar";

function App() {
  const [isLoggedIn, setIsLoggedIn] = useState(pb.authStore.isValid);

  pb.authStore.onChange(() => { 
    setIsLoggedIn(pb.authStore.isValid);
  });
  
  return (
    <>
    <NavBar></NavBar>
    <div className="flex overflow-hidden">
      {isLoggedIn && <SideBar/>}
      <div className= "flex-1 my-5 px-10">
        <Router>
          <Routes>
            <Route path="/" exact element={<Homepage/>}></Route>
            <Route path="/auth" exact element={<Auth/>} />
            <Route path="/calendar" exact element={<Calendar/>} />
            <Route path="/calendar/:id" exact element={<Calendar/>} />
            <Route path="/lessons" exact element={<Lessons/>} />
            <Route path="/lessons/:id" exact element={<Lesson/>} />
            <Route path="/students" exact element={<Students/>} />
            <Route path="/tutors" exact element={<Tutors/>} />
            <Route path="/reviews" exact element={<Reviews/>} />
            <Route path="/createLesson" exact element={<CreateLesson/>} />
            <Route path="/upcomingEvents" exact element={<UpcomingEvents/>} />
            <Route path="/upcomingEvents/student/:id" exact element={<UpcomingEvents/>} />
            <Route path="/upcomingEvents/tutor/:id" exact element={<UpcomingEvents/>} />
            <Route path="/payments" exact element={<Payments/>} />
            <Route path="/payments/student/:id" exact element={<Payments/>} />
            <Route path="/payments/tutor/:id" exact element={<Payments/>} />
            <Route path="/availability" exact element={<Availability/>} />
            <Route path="/profile" exact element={<Profile/>} />
            <Route path="/profile/:id" exact element={<Profile/>} />
            <Route path="/editProfile" exact element={<EditProfile/>} />
          </Routes>
        </Router>
      </div>
    </div>
    </>
  );
}

export default App;
