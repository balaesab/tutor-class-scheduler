
export function getDate(date) {
    return date.toString('dd.MM.yyyy');
}

export function getTime(date) {
    return date.toString('HH:mm');
}

export function formatDate(date) {
    return getDate(date) + " " + getTime(date);
}

export function getParticipantsNames(participants) {
    const participantsNames = participants.map(participant => participant.name);
    return participantsNames.join(', ');
}