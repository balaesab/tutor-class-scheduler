# Calendar for tutoring
The application is designed for students and tutors who want to either find or offer tutoring. Tutors can post a tutoring offer, which students can then sign up for by selecting a tutoring appointment using the calendar.

### Used technologies
The client side of this project is implemented using React, Tailwind and Flowbite. 
DayPilot is used to create the calendar component.

The server side is implemented using Pocketbase.



## Installing libraries
To run this project you need these libraries:

`npm install react-scripts`\
`npm install pocketbase`\
`npm install @daypilot/daypilot-lite-react`\
`npm install -S react-router-dom`\
`npm install -D tailwindcss postcss autoprefixer`\
`npm install flowbite flowbite-react`\
`npm install @heroicons/react`

## Start Pocketbase server
Run `start_pocket_base.cmd` in pocketbase folder to start the local server.

You can access:
- REST API: http://127.0.0.1:8090/api/
- Admin UI: http://127.0.0.1:8090/_/

## Start React app

Run `npm start` in the project directory.

Open [http://localhost:3000](http://localhost:3000) to view it in your browser.



