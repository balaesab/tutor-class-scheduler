migrate((db) => {
  const dao = new Dao(db)
  const collection = dao.findCollectionByNameOrId("11rl0o6hfzjv2cl")

  // add
  collection.schema.addField(new SchemaField({
    "system": false,
    "id": "b8s2hhne",
    "name": "weekday",
    "type": "text",
    "required": false,
    "unique": false,
    "options": {
      "min": null,
      "max": null,
      "pattern": ""
    }
  }))

  return dao.saveCollection(collection)
}, (db) => {
  const dao = new Dao(db)
  const collection = dao.findCollectionByNameOrId("11rl0o6hfzjv2cl")

  // remove
  collection.schema.removeField("b8s2hhne")

  return dao.saveCollection(collection)
})
