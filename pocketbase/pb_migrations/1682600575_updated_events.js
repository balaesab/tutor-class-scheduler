migrate((db) => {
  const dao = new Dao(db)
  const collection = dao.findCollectionByNameOrId("43k4w5me3q0zyep")

  collection.listRule = ""
  collection.viewRule = ""

  return dao.saveCollection(collection)
}, (db) => {
  const dao = new Dao(db)
  const collection = dao.findCollectionByNameOrId("43k4w5me3q0zyep")

  collection.listRule = null
  collection.viewRule = null

  return dao.saveCollection(collection)
})
