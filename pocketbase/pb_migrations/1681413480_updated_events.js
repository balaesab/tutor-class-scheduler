migrate((db) => {
  const dao = new Dao(db)
  const collection = dao.findCollectionByNameOrId("43k4w5me3q0zyep")

  // update
  collection.schema.addField(new SchemaField({
    "system": false,
    "id": "rocses3e",
    "name": "lesson",
    "type": "relation",
    "required": false,
    "unique": false,
    "options": {
      "collectionId": "se6h4pcq3p28ach",
      "cascadeDelete": false,
      "minSelect": null,
      "maxSelect": 1,
      "displayFields": [
        "name"
      ]
    }
  }))

  return dao.saveCollection(collection)
}, (db) => {
  const dao = new Dao(db)
  const collection = dao.findCollectionByNameOrId("43k4w5me3q0zyep")

  // update
  collection.schema.addField(new SchemaField({
    "system": false,
    "id": "rocses3e",
    "name": "field",
    "type": "relation",
    "required": false,
    "unique": false,
    "options": {
      "collectionId": "se6h4pcq3p28ach",
      "cascadeDelete": false,
      "minSelect": null,
      "maxSelect": 1,
      "displayFields": [
        "name"
      ]
    }
  }))

  return dao.saveCollection(collection)
})
