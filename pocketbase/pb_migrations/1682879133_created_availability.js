migrate((db) => {
  const collection = new Collection({
    "id": "11rl0o6hfzjv2cl",
    "created": "2023-04-30 18:25:33.443Z",
    "updated": "2023-04-30 18:25:33.443Z",
    "name": "availability",
    "type": "base",
    "system": false,
    "schema": [
      {
        "system": false,
        "id": "fkjqyz71",
        "name": "start",
        "type": "date",
        "required": false,
        "unique": false,
        "options": {
          "min": "",
          "max": ""
        }
      }
    ],
    "listRule": null,
    "viewRule": null,
    "createRule": null,
    "updateRule": null,
    "deleteRule": null,
    "options": {}
  });

  return Dao(db).saveCollection(collection);
}, (db) => {
  const dao = new Dao(db);
  const collection = dao.findCollectionByNameOrId("11rl0o6hfzjv2cl");

  return dao.deleteCollection(collection);
})
