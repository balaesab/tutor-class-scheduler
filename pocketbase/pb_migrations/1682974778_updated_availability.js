migrate((db) => {
  const dao = new Dao(db)
  const collection = dao.findCollectionByNameOrId("11rl0o6hfzjv2cl")

  // add
  collection.schema.addField(new SchemaField({
    "system": false,
    "id": "setv24tt",
    "name": "creator",
    "type": "relation",
    "required": false,
    "unique": false,
    "options": {
      "collectionId": "_pb_users_auth_",
      "cascadeDelete": false,
      "minSelect": null,
      "maxSelect": 1,
      "displayFields": []
    }
  }))

  return dao.saveCollection(collection)
}, (db) => {
  const dao = new Dao(db)
  const collection = dao.findCollectionByNameOrId("11rl0o6hfzjv2cl")

  // remove
  collection.schema.removeField("setv24tt")

  return dao.saveCollection(collection)
})
