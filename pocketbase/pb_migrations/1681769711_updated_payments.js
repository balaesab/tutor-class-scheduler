migrate((db) => {
  const dao = new Dao(db)
  const collection = dao.findCollectionByNameOrId("71v51uxyvxw5j6q")

  // add
  collection.schema.addField(new SchemaField({
    "system": false,
    "id": "bn04wvep",
    "name": "student",
    "type": "relation",
    "required": false,
    "unique": false,
    "options": {
      "collectionId": "_pb_users_auth_",
      "cascadeDelete": false,
      "minSelect": null,
      "maxSelect": 1,
      "displayFields": []
    }
  }))

  return dao.saveCollection(collection)
}, (db) => {
  const dao = new Dao(db)
  const collection = dao.findCollectionByNameOrId("71v51uxyvxw5j6q")

  // remove
  collection.schema.removeField("bn04wvep")

  return dao.saveCollection(collection)
})
