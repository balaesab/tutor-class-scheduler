migrate((db) => {
  const dao = new Dao(db)
  const collection = dao.findCollectionByNameOrId("11rl0o6hfzjv2cl")

  // add
  collection.schema.addField(new SchemaField({
    "system": false,
    "id": "poild0p6",
    "name": "end",
    "type": "date",
    "required": false,
    "unique": false,
    "options": {
      "min": "",
      "max": ""
    }
  }))

  return dao.saveCollection(collection)
}, (db) => {
  const dao = new Dao(db)
  const collection = dao.findCollectionByNameOrId("11rl0o6hfzjv2cl")

  // remove
  collection.schema.removeField("poild0p6")

  return dao.saveCollection(collection)
})
