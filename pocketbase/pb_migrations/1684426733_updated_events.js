migrate((db) => {
  const dao = new Dao(db)
  const collection = dao.findCollectionByNameOrId("43k4w5me3q0zyep")

  collection.deleteRule = "@request.auth.id != \"\" && creator = @request.auth.id || participants ?= @request.auth.id"

  return dao.saveCollection(collection)
}, (db) => {
  const dao = new Dao(db)
  const collection = dao.findCollectionByNameOrId("43k4w5me3q0zyep")

  collection.deleteRule = null

  return dao.saveCollection(collection)
})
