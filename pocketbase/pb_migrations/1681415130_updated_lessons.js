migrate((db) => {
  const dao = new Dao(db)
  const collection = dao.findCollectionByNameOrId("se6h4pcq3p28ach")

  collection.viewRule = "@request.auth.id != \"\" && creator = @request.auth.id"

  return dao.saveCollection(collection)
}, (db) => {
  const dao = new Dao(db)
  const collection = dao.findCollectionByNameOrId("se6h4pcq3p28ach")

  collection.viewRule = null

  return dao.saveCollection(collection)
})
