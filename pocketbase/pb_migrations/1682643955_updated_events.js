migrate((db) => {
  const dao = new Dao(db)
  const collection = dao.findCollectionByNameOrId("43k4w5me3q0zyep")

  // update
  collection.schema.addField(new SchemaField({
    "system": false,
    "id": "1xcga7ca",
    "name": "payment",
    "type": "relation",
    "required": false,
    "unique": false,
    "options": {
      "collectionId": "71v51uxyvxw5j6q",
      "cascadeDelete": false,
      "minSelect": null,
      "maxSelect": null,
      "displayFields": []
    }
  }))

  return dao.saveCollection(collection)
}, (db) => {
  const dao = new Dao(db)
  const collection = dao.findCollectionByNameOrId("43k4w5me3q0zyep")

  // update
  collection.schema.addField(new SchemaField({
    "system": false,
    "id": "1xcga7ca",
    "name": "payment",
    "type": "relation",
    "required": false,
    "unique": false,
    "options": {
      "collectionId": "71v51uxyvxw5j6q",
      "cascadeDelete": false,
      "minSelect": null,
      "maxSelect": 1,
      "displayFields": []
    }
  }))

  return dao.saveCollection(collection)
})
