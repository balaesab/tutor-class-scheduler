migrate((db) => {
  const dao = new Dao(db)
  const collection = dao.findCollectionByNameOrId("71v51uxyvxw5j6q")

  // update
  collection.schema.addField(new SchemaField({
    "system": false,
    "id": "15fyfftf",
    "name": "event",
    "type": "relation",
    "required": false,
    "unique": false,
    "options": {
      "collectionId": "43k4w5me3q0zyep",
      "cascadeDelete": false,
      "minSelect": null,
      "maxSelect": 1,
      "displayFields": []
    }
  }))

  return dao.saveCollection(collection)
}, (db) => {
  const dao = new Dao(db)
  const collection = dao.findCollectionByNameOrId("71v51uxyvxw5j6q")

  // update
  collection.schema.addField(new SchemaField({
    "system": false,
    "id": "15fyfftf",
    "name": "field",
    "type": "relation",
    "required": false,
    "unique": false,
    "options": {
      "collectionId": "43k4w5me3q0zyep",
      "cascadeDelete": false,
      "minSelect": null,
      "maxSelect": 1,
      "displayFields": []
    }
  }))

  return dao.saveCollection(collection)
})
