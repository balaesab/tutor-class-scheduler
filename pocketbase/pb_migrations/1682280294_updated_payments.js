migrate((db) => {
  const dao = new Dao(db)
  const collection = dao.findCollectionByNameOrId("71v51uxyvxw5j6q")

  // add
  collection.schema.addField(new SchemaField({
    "system": false,
    "id": "ofd3rhl8",
    "name": "approved",
    "type": "bool",
    "required": false,
    "unique": false,
    "options": {}
  }))

  return dao.saveCollection(collection)
}, (db) => {
  const dao = new Dao(db)
  const collection = dao.findCollectionByNameOrId("71v51uxyvxw5j6q")

  // remove
  collection.schema.removeField("ofd3rhl8")

  return dao.saveCollection(collection)
})
