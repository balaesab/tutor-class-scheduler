migrate((db) => {
  const dao = new Dao(db)
  const collection = dao.findCollectionByNameOrId("71v51uxyvxw5j6q")

  // update
  collection.schema.addField(new SchemaField({
    "system": false,
    "id": "bn04wvep",
    "name": "student",
    "type": "relation",
    "required": false,
    "unique": false,
    "options": {
      "collectionId": "_pb_users_auth_",
      "cascadeDelete": false,
      "minSelect": null,
      "maxSelect": 1,
      "displayFields": []
    }
  }))

  return dao.saveCollection(collection)
}, (db) => {
  const dao = new Dao(db)
  const collection = dao.findCollectionByNameOrId("71v51uxyvxw5j6q")

  // update
  collection.schema.addField(new SchemaField({
    "system": false,
    "id": "bn04wvep",
    "name": "recepient",
    "type": "relation",
    "required": false,
    "unique": false,
    "options": {
      "collectionId": "_pb_users_auth_",
      "cascadeDelete": false,
      "minSelect": null,
      "maxSelect": 1,
      "displayFields": []
    }
  }))

  return dao.saveCollection(collection)
})
