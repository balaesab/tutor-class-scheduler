migrate((db) => {
  const dao = new Dao(db)
  const collection = dao.findCollectionByNameOrId("se6h4pcq3p28ach")

  collection.listRule = ""
  collection.viewRule = ""

  return dao.saveCollection(collection)
}, (db) => {
  const dao = new Dao(db)
  const collection = dao.findCollectionByNameOrId("se6h4pcq3p28ach")

  collection.listRule = null
  collection.viewRule = null

  return dao.saveCollection(collection)
})
