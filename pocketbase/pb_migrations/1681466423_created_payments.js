migrate((db) => {
  const collection = new Collection({
    "id": "71v51uxyvxw5j6q",
    "created": "2023-04-14 10:00:23.596Z",
    "updated": "2023-04-14 10:00:23.596Z",
    "name": "payments",
    "type": "base",
    "system": false,
    "schema": [
      {
        "system": false,
        "id": "15fyfftf",
        "name": "field",
        "type": "relation",
        "required": false,
        "unique": false,
        "options": {
          "collectionId": "43k4w5me3q0zyep",
          "cascadeDelete": false,
          "minSelect": null,
          "maxSelect": 1,
          "displayFields": []
        }
      }
    ],
    "listRule": null,
    "viewRule": null,
    "createRule": null,
    "updateRule": null,
    "deleteRule": null,
    "options": {}
  });

  return Dao(db).saveCollection(collection);
}, (db) => {
  const dao = new Dao(db);
  const collection = dao.findCollectionByNameOrId("71v51uxyvxw5j6q");

  return dao.deleteCollection(collection);
})
