migrate((db) => {
  const dao = new Dao(db)
  const collection = dao.findCollectionByNameOrId("8q5bqdvqiwr8bh7")

  // add
  collection.schema.addField(new SchemaField({
    "system": false,
    "id": "irmu6d1t",
    "name": "response",
    "type": "text",
    "required": false,
    "unique": false,
    "options": {
      "min": null,
      "max": null,
      "pattern": ""
    }
  }))

  return dao.saveCollection(collection)
}, (db) => {
  const dao = new Dao(db)
  const collection = dao.findCollectionByNameOrId("8q5bqdvqiwr8bh7")

  // remove
  collection.schema.removeField("irmu6d1t")

  return dao.saveCollection(collection)
})
