migrate((db) => {
  const dao = new Dao(db)
  const collection = dao.findCollectionByNameOrId("513kstvi4y1senk")

  // add
  collection.schema.addField(new SchemaField({
    "system": false,
    "id": "xw2orqo9",
    "name": "creator",
    "type": "relation",
    "required": false,
    "unique": false,
    "options": {
      "collectionId": "_pb_users_auth_",
      "cascadeDelete": false,
      "minSelect": null,
      "maxSelect": 1,
      "displayFields": []
    }
  }))

  return dao.saveCollection(collection)
}, (db) => {
  const dao = new Dao(db)
  const collection = dao.findCollectionByNameOrId("513kstvi4y1senk")

  // remove
  collection.schema.removeField("xw2orqo9")

  return dao.saveCollection(collection)
})
