migrate((db) => {
  const dao = new Dao(db)
  const collection = dao.findCollectionByNameOrId("se6h4pcq3p28ach")

  // add
  collection.schema.addField(new SchemaField({
    "system": false,
    "id": "nj0pq7yj",
    "name": "duration",
    "type": "number",
    "required": false,
    "unique": false,
    "options": {
      "min": null,
      "max": null
    }
  }))

  // add
  collection.schema.addField(new SchemaField({
    "system": false,
    "id": "mxgbbnuc",
    "name": "numberOfStudents",
    "type": "number",
    "required": false,
    "unique": false,
    "options": {
      "min": null,
      "max": null
    }
  }))

  // update
  collection.schema.addField(new SchemaField({
    "system": false,
    "id": "cicjiumi",
    "name": "description",
    "type": "text",
    "required": false,
    "unique": false,
    "options": {
      "min": null,
      "max": null,
      "pattern": ""
    }
  }))

  // update
  collection.schema.addField(new SchemaField({
    "system": false,
    "id": "nctjoe7f",
    "name": "price",
    "type": "number",
    "required": false,
    "unique": false,
    "options": {
      "min": null,
      "max": null
    }
  }))

  return dao.saveCollection(collection)
}, (db) => {
  const dao = new Dao(db)
  const collection = dao.findCollectionByNameOrId("se6h4pcq3p28ach")

  // remove
  collection.schema.removeField("nj0pq7yj")

  // remove
  collection.schema.removeField("mxgbbnuc")

  // update
  collection.schema.addField(new SchemaField({
    "system": false,
    "id": "cicjiumi",
    "name": "popis",
    "type": "text",
    "required": false,
    "unique": false,
    "options": {
      "min": null,
      "max": null,
      "pattern": ""
    }
  }))

  // update
  collection.schema.addField(new SchemaField({
    "system": false,
    "id": "nctjoe7f",
    "name": "cena",
    "type": "number",
    "required": false,
    "unique": false,
    "options": {
      "min": null,
      "max": null
    }
  }))

  return dao.saveCollection(collection)
})
