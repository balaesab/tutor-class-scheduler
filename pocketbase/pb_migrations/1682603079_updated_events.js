migrate((db) => {
  const dao = new Dao(db)
  const collection = dao.findCollectionByNameOrId("43k4w5me3q0zyep")

  collection.listRule = "@request.auth.role = \"tutor\" && (@request.auth.id != \"\" && creator = @request.auth.id) || @request.auth.role = \"student\" && participants ?= @request.auth.id"

  return dao.saveCollection(collection)
}, (db) => {
  const dao = new Dao(db)
  const collection = dao.findCollectionByNameOrId("43k4w5me3q0zyep")

  collection.listRule = null

  return dao.saveCollection(collection)
})
