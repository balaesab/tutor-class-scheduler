migrate((db) => {
  const dao = new Dao(db)
  const collection = dao.findCollectionByNameOrId("se6h4pcq3p28ach")

  // add
  collection.schema.addField(new SchemaField({
    "system": false,
    "id": "cicjiumi",
    "name": "popis",
    "type": "text",
    "required": false,
    "unique": false,
    "options": {
      "min": null,
      "max": null,
      "pattern": ""
    }
  }))

  // add
  collection.schema.addField(new SchemaField({
    "system": false,
    "id": "nctjoe7f",
    "name": "cena",
    "type": "number",
    "required": false,
    "unique": false,
    "options": {
      "min": null,
      "max": null
    }
  }))

  return dao.saveCollection(collection)
}, (db) => {
  const dao = new Dao(db)
  const collection = dao.findCollectionByNameOrId("se6h4pcq3p28ach")

  // remove
  collection.schema.removeField("cicjiumi")

  // remove
  collection.schema.removeField("nctjoe7f")

  return dao.saveCollection(collection)
})
